//
//  BIDCustomPickerViewController.m
//  Pickers
//
//  Created by manu on 01/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "BIDCustomPickerViewController.h"

@interface BIDCustomPickerViewController ()

@end

@implementation BIDCustomPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
