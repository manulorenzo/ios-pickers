//
//  BIDDoubleComponentPickerViewController.h
//  Pickers
//
//  Created by manu on 01/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kFillingComponent 0
#define kBreadComponent 1

@interface BIDDoubleComponentPickerViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) IBOutlet UIPickerView *doublePicker;
@property (nonatomic, strong) NSArray *fillingTypes;
@property (nonatomic, strong) NSArray *breadTypes;

-(IBAction)buttonPressed;

@end
