//
//  main.m
//  Pickers
//
//  Created by manu on 01/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BIDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BIDAppDelegate class]));
    }
}
