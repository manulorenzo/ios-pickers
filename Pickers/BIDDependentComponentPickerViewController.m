//
//  BIDDependentComponentPickerViewController.m
//  Pickers
//
//  Created by manu on 01/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "BIDDependentComponentPickerViewController.h"

@interface BIDDependentComponentPickerViewController ()

@end

@implementation BIDDependentComponentPickerViewController

-(IBAction)buttonPressed {
    NSInteger cityRow = [self.dependentPicker selectedRowInComponent:kCityComponent];
    NSInteger zipRow = [self.dependentPicker selectedRowInComponent:kZipComponent];
    
    NSString *city = self.cities[cityRow];
    NSString *zip = self.zips[zipRow];
    
    NSString *title = [[NSString alloc] initWithFormat:@"You selected the zip code %@", zip];
    NSString *message = [[NSString alloc] initWithFormat:@"%@ is in %@", zip, city];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Thank you!" otherButtonTitles:nil, nil];
    
    [alert show];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *plistURL = [bundle URLForResource:@"statedictionary" withExtension:@"plist"];
    
    self.cityZips = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    
    NSArray *allCities = [self.cityZips allKeys];
    NSArray *sortedStates = [allCities sortedArrayUsingSelector:@selector(compare:)];
    
    self.cities = sortedStates;
    
    NSString *selectedCity = self.cities[0];
    self.zips = self.cityZips[selectedCity];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Picker Data Source Methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == kCityComponent) {
        return [self.cities count];
    } else {
        return [self.zips count];
    }
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == kCityComponent) {
        return self.cities[row];
    } else {
        return self.zips[row];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == kCityComponent) {
        NSString *selectedCity = self.cities[row];
        self.zips = self.cityZips[selectedCity];
        [self.dependentPicker reloadComponent:kZipComponent];
        [self.dependentPicker selectRow:row inComponent:kCityComponent animated:YES];
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == kZipComponent) {
        return 90;
    } else {
        return 200;
    }
}

@end
