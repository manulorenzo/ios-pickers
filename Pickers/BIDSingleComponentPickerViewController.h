//
//  BIDSingleComponentPickerViewController.h
//  Pickers
//
//  Created by manu on 01/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDSingleComponentPickerViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) IBOutlet UIPickerView *singlePicker;
@property (nonatomic, strong) NSArray *characterNames;
-(IBAction)buttonPressed;

@end
