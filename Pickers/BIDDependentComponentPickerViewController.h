//
//  BIDDependentComponentPickerViewController.h
//  Pickers
//
//  Created by manu on 01/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kCityComponent 0
#define kZipComponent 1

@interface BIDDependentComponentPickerViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIPickerView *dependentPicker;
@property (strong, nonatomic) NSDictionary *cityZips;
@property (strong, nonatomic) NSArray *cities;
@property (strong, nonatomic) NSArray *zips;
-(IBAction)buttonPressed;

@end
