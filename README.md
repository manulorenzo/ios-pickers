iOS-Pickers
===========

Exercises from the Chapter 7 of the book Beginning iOS6 Development (http://www.apress.com/9781430245124). 
This is a simple example meant to show the basics of having dealing with the different types of Pickers we could have in iOS (single, dependent, independent, custom, etc).
